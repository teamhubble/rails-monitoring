module RailsMonitoring
  class Engine < ::Rails::Engine
    isolate_namespace(RailsMonitoring)
  end
end
