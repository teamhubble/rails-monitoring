$:.push File.expand_path("lib", __dir__)

require('rails_monitoring/version')

Gem::Specification.new do |spec|
  spec.name = 'rails-monitoring'
  spec.version = RailsMonitoring::VERSION
  spec.authors = ['Alexandre Zicat']
  spec.email = ['alexzicat@teamhubble.com']

  spec.summary = "Monitoring for Hubble's Rails sites."
  spec.description = "Monitoring for Hubble's Rails sites."
  spec.homepage = 'https://bitbucket.org/teamhubble/rails-monitoring'
  spec.license = 'MIT'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency('rails', '>= 4')
end
